//
//  JKCenterScrollView.m
//  ImageTransition
//
//  Created by Joris Kluivers on 1/14/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import "JKCenterScrollView.h"

@implementation JKCenterScrollView

- (void) layoutSubviews
{
	[super layoutSubviews];
	
	UIView *centerView = [self viewWithTag:JKCenterViewTag];
	if (!centerView) {
		return;
	}
	
	CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = centerView.frame;
	
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
	
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
	
    centerView.frame = frameToCenter;
}

@end
